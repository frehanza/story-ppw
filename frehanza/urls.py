from django.urls import path,include
from . import views
from .views import (
  index,
  create,
  detail,
  delete,
)

app_name = 'frehanza'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('scheduler/', create, name='forms'),
    path('scheduler/list/', index, name='index'),
    path('scheduler/details/<int:my_id>', detail, name='detail'),
    path('scheduler/delete/<int:my_id>', delete, name='delete')
]

