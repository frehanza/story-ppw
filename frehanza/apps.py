from django.apps import AppConfig


class FrehanzaConfig(AppConfig):
    name = 'frehanza'
