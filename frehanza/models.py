from django.db import models

# Create your models here.
class Scheduler(models.Model):
  course = models.CharField(max_length=128)
  lecturer = models.CharField(max_length=128)
  credits = models.IntegerField(default=0)
  description = models.TextField(max_length=128)
  term = models.CharField(max_length=128)
  room = models.CharField(max_length=128)



  def __str__(self):
    return f'{self.course} ({self.credits} sks) : {self.lecturer} '
