from django.contrib import admin
from django.db import models

from .models import Scheduler

# Register your models here.
admin.site.register(Scheduler)