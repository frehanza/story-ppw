from django.shortcuts import render, redirect
from django.urls import reverse

# Models
from .models import Scheduler

# Form
from .forms import SchedulerForm

# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html') 

def index(request):
    course_list = Scheduler.objects.all()
    context={
    "course_list": course_list
    }
    return render(request, "index.html", context)
 

def create(request):
  print("Masuk Create")
  success_notif = ''
  error_notif = ''
  if (request.method == 'POST') :
    print("Masuk Post")
    form = SchedulerForm(request.POST)
    if form.is_valid():
      print("Masuk valid")
      new_scheduler = Scheduler(
        course = request.POST['course'],
        lecturer = request.POST['lecturer'],
        credits = request.POST['credits'],
        description = request.POST['description'],
        term = request.POST['term'],
        room = request.POST['room']
      )
      new_scheduler.save()
      success_notif = 'Successfully Added!'
      return redirect('/scheduler/list/')
    else:
      error_notif = 'Failed!'
  else:
    form = SchedulerForm()

  context = {
    'form': form,
  }
  return render(request,'forms.html', context)

def detail(request, my_id):
  scheduler = Scheduler.objects.get(id = my_id)
  context = {
    "scheduler": scheduler,
  }
  return render(request, "details.html", context)

def delete(request, my_id):
  scheduler = Scheduler.objects.get(id = my_id)
  scheduler.delete()
  return redirect('/scheduler/list/')




