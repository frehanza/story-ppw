from django.forms import ModelForm
from .models import Scheduler

class SchedulerForm(ModelForm):
    class Meta:
        model = Scheduler
        fields = '__all__'